//Init made for the helidoors by eRazeri
{
if (
(typeOf _x == "RG_Merlin_CTRG")
||
(typeOf _x == "RG_Merlin_DS")
||
(typeOf _x == "RG_Merlin_WD")
||
(typeOf _x == "RG_Ghosthawk_CTRG")
||
(typeOf _x == "RG_Ghosthawk_DS")
||
(typeOf _x == "RG_Ghosthawk_WD")
||
(typeOf _x == "B_Heli_Transport_01_F")
||
(typeOf _x == "B_Heli_Transport_01_camo_F")
||
(typeOf _x == "I_Heli_Transport_02_F")
) then {
[_x] execVM "scripts\helidoors\b2_heliDoors.sqf";
};
} forEach vehicles;