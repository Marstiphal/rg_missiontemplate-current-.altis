//wip RG task force tracker function
//Done by eRazeri
//initial function from qbt, thanks once again mate!
rg_fnc_trackerDrawMap = {

if ((typeOf player) == "RG_GameAdmin_DS" || (typeOf player) == "RG_GameAdmin_WD") then {
        RG_tracker_classnames_to_track = RG_tracker_classnames_to_track + ["RG_GameAdmin_DS","RG_GameAdmin_WD"];
        };
        
    if ((typeOf player) in RG_tracker_classnames_to_track) then {
        {
            if (((typeOf _x) in RG_tracker_classnames_to_track) && (side (group player) == side (group _x))) then {
                if (_x == ((crew (vehicle _x)) select 0))  then {

                    _texture = switch (true) do {
                        case (vehicle _x isKindOf "Man"): {"\a3\ui_f\data\map\Markers\NATO\b_inf.paa"};
                        case (vehicle _x isKindOf "Car"): {"\a3\ui_f\data\map\Markers\NATO\b_motor_inf.paa"};
                        case (vehicle _x isKindOf "Helicopter"): {"\a3\ui_f\data\map\Markers\NATO\b_air.paa"};
                        case (vehicle _x isKindOf "Plane"): {"\a3\ui_f\data\map\Markers\NATO\b_plane.paa"};
                        case (vehicle _x isKindOf "Tank"): {"\a3\ui_f\data\map\Markers\NATO\b_armor.paa"};
                        case (vehicle _x isKindOf "Ship"): {"\a3\ui_f\data\map\Markers\NATO\b_naval.paa"};
                        default {"\a3\ui_f\data\map\Markers\NATO\b_unknown.paa"};
                        };

                    _colorIconLocal = side (group _x)  call bis_fnc_sidecolor;
                    

                    _strText = switch (true) do {
                        case (vehicle _x isKindOf "Man"):{groupID (group _x)};
                        case (vehicle _x isKindOf "Car"):{groupID (group _x)};
                        case (vehicle _x isKindOf "Tank"):{groupID (group _x)};
                        case (vehicle _x isKindOf "Ship"):{groupID (group _x)};
                        case (vehicle _x isKindOf "Helicopter"):
                        {
                            switch (true) do {
                                case (vehicle _x isKindOf "CH_47F_EP1"): {"Atlas"};
                                case (vehicle _x isKindOf "RG_Ghosthawk_CTRG"): {"Phantom"};
                                case (vehicle _x isKindOf "RG_Ghosthawk_DS"): {"Phantom"};
                                case (vehicle _x isKindOf "RG_Ghosthawk_WD"): {"Phantom"};
                                case (vehicle _x isKindOf "RG_Comanche_CTRG"): {"Hitman"};
                                case (vehicle _x isKindOf "RG_Comanche_DS"):{"Hitman"};
                                case (vehicle _x isKindOf "RG_Comanche_WD"):{"Hitman"};
                                case (vehicle _x isKindOf "RG_Wildcat_CTRG"):{"Wasp"};
                                case (vehicle _x isKindOf "RG_Wildcat_DS"):{"Wasp"};
                                case (vehicle _x isKindOf "RG_Wildcat_WD"):{"Wasp"};
                                case (vehicle _x isKindOf "RG_Wildcat_unarmed_CTRG"):{"Stalker"};
                                case (vehicle _x isKindOf "RG_Wildcat_unarmed_DS"):{"Stalker"};
                                case (vehicle _x isKindOf "RG_Wildcat_unarmed_WD"):{"Stalker"};
                                case (vehicle _x isKindOf "RG_Merlin_CTRG"):{"Merlin"};
                                case (vehicle _x isKindOf "RG_Merlin_DS"):{"Merlin"};
                                case (vehicle _x isKindOf "RG_Merlin_WD"):{"Merlin"};
                                default {groupID (group _x)};
                                };
                        };
                        case (vehicle _x isKindOf "Plane"):
                        {
                            switch (true) do {
                                case (vehicle _x isKindOf "C130J_ported"):{"Condor"};
                                case (vehicle _x isKindOf "MV22_ported"):{"Condor"};
                                case (vehicle _x isKindOf "RG_Warthog"):{"Reaper"};
                                case (vehicle _x isKindOf "JS_JC_FA18E"):{"Valkyrie"};
                                case (vehicle _x isKindOf "JS_JC_FA18F"):{"Griffin"};
                                default {groupID (group _x)};
                                };
                        };
                        };


                    (_this select 0) drawIcon [
                        _texture,

                        _colorIconLocal,

                        visiblePosition (vehicle _x),

                        28 min ((2 max (ctrlMapScale (_this select 0))) * 100),

                        28 min ((2 max (ctrlMapScale (_this select 0))) * 100),

                        0,

                        _strText,

                        0,

                        0.033,

                        "TahomaB"
                    ];
                };
            };
        } forEach playableUnits;
    };
};//END of function