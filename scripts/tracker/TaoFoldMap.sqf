//wip RG task force tracker for Tao Folding Map
//Done by eRazeri
//initial system from qbt, thanks once again mate!
disableSerialization;
while {true} do {
waitUntil {sleep .1; !(isNull (uiNamespace getVariable "Tao_FoldMap"))};
((uiNamespace getVariable "Tao_FoldMap") displayCtrl 40) ctrlAddEventHandler ["Draw", "_this call rg_fnc_trackerDrawMap"];
waitUntil {sleep .1; (isNull (uiNamespace getVariable "Tao_FoldMap"))};
};