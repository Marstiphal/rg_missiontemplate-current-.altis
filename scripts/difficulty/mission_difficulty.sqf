//This script checks the difficulty mode and shows an hint if it`s not the standard one (currently regular)
//Created by eRazeri
if (hasInterface) then {
_difficulty = switch (difficulty) do {
    case 0: {"Recruit"};
    case 1: {"Regular"};
    case 2: {"Veteran"};
    case 3: {"Elite"};
    default {"ERROR"};
};

_debug = _this select 0;

if (_debug) then {
diag_log format ["Difficulty mode: %1 = %2",difficulty,_difficulty];
diag_log format ["3rdPersonView: %1", difficultyEnabled "3rdPersonView"];
diag_log format ["armor: %1", difficultyEnabled "armor"];
diag_log format ["autoSpot: %1", difficultyEnabled "autoSpot"];
diag_log format ["autoGuideAT: %1", difficultyEnabled "autoGuideAT"];
diag_log format ["autoAim: %1", difficultyEnabled "autoAim"];
diag_log format ["allowSeagull: %1", difficultyEnabled "allowSeagull"];
diag_log format ["CameraShake: %1", difficultyEnabled "CameraShake"];
diag_log format ["clockIndicator: %1", difficultyEnabled "clockIndicator"];
diag_log format ["deathMessages: %1", difficultyEnabled "deathMessages"];
diag_log format ["enemyTag: %1", difficultyEnabled "enemyTag"];
diag_log format ["ExtendetInfoType: %1", difficultyEnabled "ExtendetInfoType"];
diag_log format ["friendlyTag: %1", difficultyEnabled "friendlyTag"];
diag_log format ["MineTag: %1", difficultyEnabled "MineTag"];
diag_log format ["hud: %1", difficultyEnabled "hud"];
diag_log format ["hudPerm: %1", difficultyEnabled "hudPerm"];
diag_log format ["hudWp: %1", difficultyEnabled "hudWp"];
diag_log format ["hudWpPerm: %1", difficultyEnabled "hudWpPerm"];
diag_log format ["HUDGroupInfo: %1", difficultyEnabled "HUDGroupInfo"];
diag_log format ["map: %1", difficultyEnabled "map"];
diag_log format ["netStats: %1", difficultyEnabled "netStats"];
diag_log format ["suppressPlayer: %1", difficultyEnabled "suppressPlayer"];
diag_log format ["tracers: %1", difficultyEnabled "tracers"];
diag_log format ["realisticFatigue: %1", difficultyEnabled "realisticFatigue"];
diag_log format ["ultraAI: %1", difficultyEnabled "ultraAI"];
diag_log format ["unlimitedSaves: %1", difficultyEnabled "unlimitedSaves"];
diag_log format ["VonID: %1", difficultyEnabled "VonID"];
diag_log format ["weaponCursor: %1", difficultyEnabled "weaponCursor"];
hint format ["Difficulty mode %1 debug written to RPT!",_difficulty];
sleep 5; 
};
if (difficulty != 1) then {
hint format ["You're using difficulty mode %1! You should be on Regular as standard!",_difficulty];
diag_log format ["Difficulty mode: %1 = %2",difficulty,_difficulty];
};
};