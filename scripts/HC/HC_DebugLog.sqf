_headLessClient = _this;
_arrayWithLocalUnits = [];
_arrayWithremoteUnits = [];

if (_headLessClient != player) exitwith {};

while {true} do {
	{
		if (local _x) then {
			_arrayWithLocalUnits set [count _arrayWithLocalUnits, _x];
		} else {
			_arrayWithremoteUnits set [count _arrayWithremoteUnits, _x];
		};
		
	}foreach allunits;
	_localAICount = count _arrayWithLocalUnits;
	_remoteAICount = count _arrayWithremoteUnits;

_client = _this;
_avgFrames = diag_fps;
_currentFrame = diag_fpsmin;

_client globalchat format["CLIENT: %1 | TIME: %2 | AVG FRAMES: %3 | LOWEST FRAME: %4 | LOCAL AI: %5 | REMOTE AI: %6",_client, time, _avgFrames, _currentFrame,  _localAICount, _remoteAICount];

	_arrayWithLocalUnits = [];
	_arrayWithremoteUnits = [];
	sleep 5;
};