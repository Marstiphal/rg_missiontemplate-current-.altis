/*
=BTC=_logistic_init.sqf
Created by =BTC= Giallustio
Version: 0.14 rc 1
Date: 20/03/2013
Visit us at: http://www.blacktemplars.altervista.org/
Additions by Beerkan 02/06/2013. Beta.0.5 including
Updated for latest Arma 3 version 1.20.124746 Vehicles and Helo's
Pops smoke on ground when cargo lands.
Deploys appropriate parachute/s if dropped above 30m.
    Deploy's a small chute for Cargo and ammocrates.
    Deploy's a single Large Chute for Cars and small boats Trucks etc.
    Deploy's multiple chutes for heavy cargo, i.e. Tanks, Large trucks, large boats etc. // From
Does NOT deploy smoke if Vehicle Cargo is occupied so to not give away position to enemy.
Pilot needs to have a 4/kph or less speed differential to pickup cargo.


[B]Beta 0.5 UPDATE[/B]

Changelog:
Re-defined pickup arrays. (Again!) adding many more items. You can add or subtract your own items to these arrays to suit your preferences. 
Made fixes for stupid ArmA3 bugs.
Certain cargo types will now orientate correctly when picked up.
Dropping items below 30m will NOT deploy chutes, and should also not suffer damage when landing.
if (isServer) then 
{
    BTC_id_repo = 10;publicVariable "BTC_id_repo";
    BTC_cargo_repo = "Land_HBarrierBig_F" createVehicle [- 5000,- 5000,0];publicVariable "BTC_cargo_repo";
};
*/

if (hasInterface) then {
BTC_active_lift      = 1;
BTC_active_fast_rope = 1;
BTC_active_cargo     = 0;//disabled cargo system
//Common
BTC_dir_action = "scripts\=BTC=_logistic\=BTC=_addAction.sqf";
BTC_l_placement_area = 20;
if (BTC_active_lift == 1) then
{
    //Lift
    BTC_lift_pilot    = ["RG_PilotHeli_DS","RG_PilotHeli_WD","RG_PilotLead_WD","RG_PilotLead_DS","RG_PilotJet_DS","RG_PilotJet_WD","RG_GameAdmin_DS","RG_GameAdmin_WD"];// set this = []; if you want any unit to use BTC_Lift
    BTC_lift          = 1;
    BTC_lifted        = 0;
    BTC_lift_min_h    = 7;// Decrease this to 2 make it easier to lift small cargo
    BTC_lift_max_h    = 12;
    BTC_lift_radius   = 3;// Increase this to 5 make it easier to lift cargo
    BTC_def_hud       = 1;
    BTC_def_pip       = 0;//1
    BTC_l_def_veh_pip = ["B_Heli_Light_01_F","I_Heli_light_03_unarmed_F","O_Heli_Light_02_unarmed_F","B_Heli_Transport_01_F","B_Heli_Transport_01_camo_F","I_Heli_Transport_02_F","CH_147F","CH_47F","RG_Ghosthawk_CTRG","RG_Ghosthawk_DS","RG_Ghosthawk_WD","RG_Wildcat_CTRG","RG_Wildcat_DS","RG_Wildcat_WD","RG_Wildcat_unarmed_CTRG","RG_Wildcat_unarmed_DS","RG_Wildcat_unarmed_WD","RG_Merlin_CTRG","RG_Merlin_DS","RG_Merlin_WD","MV22_ported","CH_47F_EP1"];
    BTC_l_pip_cond    = false;
    BTC_cargo_lifted  = objNull;
    BTC_Hud_Cond      = false;
    BTC_HUD_x         = (SafeZoneW+2*SafeZoneX) - 0.155;//+ 0.045;
    BTC_HUD_y         = (SafeZoneH+2*SafeZoneY) + 0.045;
    _paradeployed = 0;
    _lift = [] execVM "scripts\=BTC=_logistic\=BTC=_lift\=BTC=_lift_init.sqf";

// Specifiy what class of Chopper can lift which type of cargo
BTC_get_liftable_array =
    {
        _chopper = _this select 0;
        _array   = [];
        BTC_AmmoCrates = ["B_supplyCrate_F","Box_East_Ammo_F","Box_East_AmmoOrd_F","Box_East_AmmoVeh_F","Box_East_Grenades_F","Box_East_Support_F","Box_East_Wps_F",
        "Box_East_WpsLaunch_F","Box_East_WpsSpecial_F","Box_IND_Ammo_F","Box_IND_AmmoOrd_F","Box_IND_AmmoVeh_F","Box_IND_Grenades_F","Box_IND_Support_F","Box_IND_Wps_F",
        "Box_IND_WpsLaunch_F","Box_IND_WpsSpecial_F","Box_NATO_Ammo_F","Box_NATO_AmmoOrd_F","Box_NATO_AmmoVeh_F","Box_NATO_Grenades_F","Box_NATO_Support_F","Box_NATO_Wps_F",
        "Box_NATO_WpsLaunch_F","Box_NATO_WpsSpecial_F","C_supplyCrate_F","I_supplyCrate_F","IG_supplyCrate_F","O_supplyCrate_F",
        "RG_Gambler_Resupply","RG_Vegas_Resupply","RG_Talon_Ground_Resupply","RG_Radios_and_gadgets","RG_Diving_Gear","RG_Clothes_DS","RG_Clothes_WD","RG_Berets","RG_Parachutes"]; 
                   
        BTC_LightVehicles = ["B_Quadbike_01_F","C_Quadbike_01_F","I_Quadbike_01_F","O_Quadbike_01_F","C_Rubberboat","O_Rubberboat","B_Rubberboat","Motorcycle","C_Kart_01_Blu_F",
        "C_Kart_01_Fuel_F","C_Kart_01_F","C_Kart_01_Red_F","C_Kart_01_Vrana_F"];
               
        BTC_MediumVehicles = ["Car","Wheeled_APC","C_Boat_Civil_01_F","C_Boat_Civil_01_rescue_F","C_Boat_Civil_01_police_F","B_UAV_02_CAS_F","O_UAV_02_CAS_F","I_UAV_02_CAS_F",
        "B_UAV_02_F","O_UAV_02_F","I_UAV_02_F","O_UGV_01_F","O_UGV_01_rcws_F","I_UGV_01_F","B_UGV_01_F","I_UGV_01_rcws_F","B_UGV_01_rcws_F"];
                
        BTC_HeavyVehicles =["B_MBT_01_arty_F","B_MBT_01_cannon_F","I_MBT_01_cannon_F","B_MBT_01_mlrs_F","B_MBT_01_TUSK_F","B_APC_Tracked_01_rcws_F","B_APC_Tracked_01_CRV_F",
        "B_APC_Tracked_01_AA_F","B_APC_Wheeled_01_cannon_F","B_Truck_01_covered_F","B_Truck_01_transport_F","O_APC_Tracked_02_AA_F","O_APC_Tracked_02_cannon_F","O_APC_Wheeled_02_rcws_F",
        "O_MBT_02_arty_F","O_MBT_02_cannon_F","I_Truck_01_covered_F","I_Truck_01_transport_F","O_Truck_01_covered_F","O_Truck_01_transport_F","I_Boat_Armed_01_minigun_F",
        "B_Boat_Armed_01_minigun_F","O_Boat_Armed_01_hmg_F","B_Truck_01_transport_F","B_Truck_01_covered_F","B_Truck_01_box_F","B_Truck_01_Repair_F","B_Truck_01_ammo_F",
        "B_Truck_01_fuel_F","B_Truck_01_medical_F","B_Truck_01_mover_F","O_Truck_03_transport_F","O_Truck_03_covered_F","O_Truck_03_box_F","O_Truck_03_Repair_F","O_Truck_03_ammo_F",
        "O_Truck_03_fuel_F","O_Truck_03_medical_F","O_Truck_03_device_F","O_Truck_03_covered_F"];
                
        BTC_Static_Items = ["I_HMG_01_F","B_HMG_01_high_F","O_HMG_01_high_F","I_HMG_01_high_F","B_HMG_01_A_F","O_HMG_01_A_F","I_HMG_01_A_F","B_static_AA_F","O_static_AA_F","I_static_AA_F",
        "B_static_AT_F","O_static_AT_F","I_static_AT_F","B_GMG_01_F","O_GMG_01_F","I_GMG_01_F","B_GMG_01_high_F","O_GMG_01_high_F","I_GMG_01_high_F","B_GMG_01_A_F","O_GMG_01_A_F",
        "I_GMG_01_A_F","B_Mortar_01_F","O_Mortar_01_F","I_Mortar_01_F","B_G_Mortar_01_F"];
        
        BTC_Fortifications_Small = ["Land_HBarrier_1_F","Land_Shoot_House_Wall_F","Land_Shoot_House_Wall_Stand_F","Land_Shoot_House_Corner_Stand_F","Land_Shoot_House_Wall_Crouch_F",
        "Land_Shoot_House_Corner_Crouch_F","Land_Shoot_House_Wall_Prone_F","Land_Shoot_House_Corner_Prone_F","Land_Shoot_House_Wall_Long_F",
        "Land_Shoot_House_Wall_Long_Stand_F","Land_Shoot_House_Wall_Long_Crouch_F","Land_Shoot_House_Wall_Long_Prone_F","Land_Shoot_House_Corner_F",
        "Land_Shoot_House_Panels_F","Land_Shoot_House_Panels_Crouch_F","Land_Shoot_House_Panels_Prone_F","Land_Shoot_House_Panels_Vault_F",
        "Land_Shoot_House_Panels_Window_F","Land_Shoot_House_Panels_Windows_F","Land_Shoot_House_Tunnel_F","Land_Shoot_House_Tunnel_Stand_F",
        "Land_Shoot_House_Tunnel_Crouch_F","Land_Shoot_House_Tunnel_Prone_F","Land_CncBarrier_F","Land_CncBarrier_stripes_F"];
                
        BTC_Fortifications_Medium = ["Land_HBarrier_3_F","Land_ToiletBox_F","Land_FieldToilet_F","Land_CinderBlocks_F","Land_CncBarrierMedium_F","Land_CncBarrierMedium4_F",
        "Land_CncWall1_F","Land_CncWall4_F","Land_Concrete_SmallWall_4m_F","Land_Concrete_SmallWall_8m_F","Land_Mil_ConcreteWall_F","Land_Mil_WallBig_4m_F","Land_Mil_WallBig_Corner_F",
        "Land_Wall_IndCnc_2deco_F","Land_Wall_IndCnc_4_D_F"] + BTC_Fortifications_Small;
        
        BTC_Fortifications_Large = ["Land_HBarrier_5_F","Land_HBarrierBig_F"] + BTC_Fortifications_Medium;
        
        BTC_Custom_Items_Small = ["Land_PowerGenerator_F","Land_MetalBarrel_empty_F","Land_WaterBarrel_F","Land_WaterTank_F","Land_BarrelEmpty_F","Land_BarrelSand_F",
        "Land_BarrelTrash_F","Land_BarrelWater_F","Land_MetalBarrel_F","Land_WoodenBox_F"];
        
        BTC_Custom_Items_Medium = ["Land_Tank_rust_F","Land_CargoBox_V1_F"] + BTC_Custom_Items_Small;
        
        BTC_Custom_Items_Large = ["Land_Cargo20_blue_F","Land_Cargo20_brick_red_F","Land_Cargo20_cyan_F","Land_Cargo20_grey_F","Land_Cargo20_light_blue_F","Land_Cargo20_light_green_F",
        "Land_Cargo20_military_green_F","Land_Cargo20_orange_F","Land_Cargo20_red_F","Land_Cargo20_sand_F","Land_Cargo20_white_F","Land_Cargo20_yellow_F","Land_Cargo40_blue_F",
        "Land_Cargo40_brick_red_F","Land_Cargo40_cyan_F","Land_Cargo40_grey_F","Land_Cargo40_light_blue_F","Land_Cargo40_light_green_F","Land_Cargo40_military_green_F",
        "Land_Cargo40_orange_F","Land_Cargo40_red_F","Land_Cargo40_sand_F","Land_Cargo40_white_F","Land_Cargo40_yellow_F"] + BTC_Custom_Items_Medium;        
        
        switch (typeOf _chopper) do 
        {
        case "B_Heli_Light_01_F"             : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_Fortifications_Small + BTC_Custom_Items_Small;};
        case "I_Heli_light_03_unarmed_F"    : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_Fortifications_Small + BTC_Custom_Items_Medium;};
        case "O_Heli_Light_02_unarmed_F"    : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_Fortifications_Medium + BTC_Custom_Items_Medium;};
        case "B_Heli_Transport_01_F"         : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
        case "B_Heli_Transport_01_camo_F"    : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
        case "I_Heli_Transport_02_F"         : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_HeavyVehicles
        + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
        case "CH_147F"                         : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_HeavyVehicles
        + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
        case "CH_47F"                         : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_HeavyVehicles
        + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
        
        //Wildcat      
			case "RG_Wildcat_CTRG"     : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_Fortifications_Small + BTC_Custom_Items_Small;};
			case "RG_Wildcat_DS"     : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_Fortifications_Small + BTC_Custom_Items_Small;};
			case "RG_Wildcat_WD"     : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_Fortifications_Small + BTC_Custom_Items_Small;};
			case "RG_Wildcat_unarmed_CTRG"     : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_Fortifications_Small + BTC_Custom_Items_Medium;};
			case "RG_Wildcat_unarmed_DS"     : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_Fortifications_Small + BTC_Custom_Items_Medium;};
			case "RG_Wildcat_unarmed_WD"     : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_Fortifications_Small + BTC_Custom_Items_Medium;};
      
			//Ghosthawk
			case "RG_Ghosthawk_CTRG" : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
			case "RG_Ghosthawk_DS" : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
			case "RG_Ghosthawk_WD" : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
      
			//Merlin
			case "RG_Merlin_CTRG" : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_HeavyVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
			case "RG_Merlin_DS" : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_HeavyVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
			case "RG_Merlin_WD" : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_HeavyVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};

			//Osprey
			case "MV22_ported" : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_HeavyVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
			//Chinook
			case "CH_47F_EP1" : {_array = BTC_AmmoCrates + BTC_Static_Items + BTC_LightVehicles + BTC_MediumVehicles + BTC_HeavyVehicles + BTC_Fortifications_Large + BTC_Custom_Items_Large;};
  
        };
        _array
    };
};

if (BTC_active_fast_rope == 1) then
{
    //Fast roping
    BTC_fast_rope_h = 35;
    BTC_fast_rope_h_min = 5;
    BTC_roping_chopper = ["B_Heli_Light_01_F","I_Heli_light_03_unarmed_F","O_Heli_Light_02_unarmed_F","B_Heli_Transport_01_F","B_Heli_Transport_01_camo_F","I_Heli_Transport_02_F","CH_147F","CH_147F","RG_Ghosthawk_CTRG","RG_Ghosthawk_DS","RG_Ghosthawk_WD","RG_Wildcat_CTRG","RG_Wildcat_DS","RG_Wildcat_WD","RG_Wildcat_unarmed_CTRG","RG_Wildcat_unarmed_DS","RG_Wildcat_unarmed_WD","RG_Merlin_CTRG","RG_Merlin_DS","RG_Merlin_WD","MV22_ported","CH_47F_EP1"];
    _rope = [] execVM "scripts\=BTC=_logistic\=BTC=_fast_roping\=BTC=_fast_roping_init.sqf";
};

if (BTC_active_cargo == 1) then
{
    //Cargo System
    _cargo = [] execVM "scripts\=BTC=_logistic\=BTC=_cargo_system\=BTC=_cargo_system_init.sqf";
    BTC_def_vehicles     = ["LandVehicle","Helicopter","Ship"];
    BTC_def_cargo        = ["Motorcycle","ReammoBox","ReammoBox_F","Strategic"];
    BTC_def_drag         = ["ReammoBox","ReammoBox_F","Strategic"];
    BTC_def_placement    = ["ReammoBox","ReammoBox_F","Strategic"];
    BTC_cargo_selected   = objNull;
    BTC_def_cc =
    [
        "C_Rubberboat",2,
        "O_Rubberboat",2,
        "B_Rubberboat",2,
        "B_Quadbike_01_F",2,
        "C_Quadbike_01_F",2,
        "I_Quadbike_01_F",2,
        "O_Quadbike_01_F",2,
        "Car",4,
        "Tank",6,
        "Wheeled_APC",6,
        "Tracked_APC",6,        
         "B_Truck_01_transport_F",10,
        "B_Truck_01_covered_F",10,
        "I_Truck_02_covered_F",10,
        "O_Truck_02_covered_F",10,
        "I_Truck_02_transport_F",10,
        "O_Truck_02_transport_F",10,
        "O_Truck_03_transport_F",14,
        "O_Truck_03_covered_F",12,
        "O_Truck_03_box_F",12
    ];
    BTC_def_rc =
    [
        "Land_BagBunker_Small_F",4
    ];
};

//Functions

BTC_No_chute =
{    private ["_payload","_smoke"];
    BTC_lifted = 0;
    0 = [_this] spawn {
    _payload = _this select 0;
    _height = (getPos _payload) select 2;
    _fall   = 0.09;
    // Deploy smoke but only if there is no one (alive) inside.
    if ({alive _x} count crew BTC_cargo_lifted == 0)
        then {
        _smoke = "SmokeShellGreen" createVehicle position _payload;
        _smoke setPos (getPos _payload);
        _smoke attachTo [_payload,[0,0,0]];
            };    
    while {((getPos _payload) select 2) > 0.1} do 
            {
        _fall = (_fall * 1.05);// Visually closer to real gravity decent.
        _payload setPos [getPos _payload select 0, getPos _payload select 1, _height];
        _height = _height - _fall;
        sleep 0.01;
            };
        BTC_cargo_lifted = ObjNull;
    };
};

BTC_Fnc_NonSteerPara = {
        private ["_para","_paras","_payload","_smoke","_paradeployed"];
        _paradeployed = 1;
        _height = (getPos BTC_cargo_lifted) select 2;
        _fall   = 0.09;
        BTC_cargo_lifted setvariable ["BTC_cannot_lift",1,false];
        // Force item to fall. Some small items & classes don't have physx or understand gravity. (GAME BUG!!) Wait until it's 15m away then deploy chute.
        while {BTC_cargo_lifted distance _chopper < 20} do 
            {
        _fall = (_fall * 1.1);
        BTC_cargo_lifted setPos [getPos BTC_cargo_lifted select 0, getPos BTC_cargo_lifted select 1, _height];
        _height = _height - _fall;
        sleep 0.01;
            };        
        BTC_cargo_lifted setvariable ["BTC_cannot_lift",0,false];
        BTC_lifted = 0;
        _para = createVehicle ["NonSteerable_Parachute_F", [0,0,0], [], 0, "FLY"];
        _para disableCollisionWith _chopper; 
        _para setDir getDir _this;
        _para setPos getPos _this;
        _paras =  [_para];
        _this attachTo [_para, [0,0,0]];
        0 = [_this, _paras] spawn {
            _payload = _this select 0;
            // Wait until near ground then deploy smoke but only if there is no one (alive) inside.
            waitUntil {(getPos _payload select 2) < 25};
            if ({alive _x} count crew _payload == 0)
            then {
            _smoke = "SmokeShellGreen" createVehicle position _payload;
            _smoke setPos (getPos _payload);
            _smoke attachTo [_payload,[0,0,0]];
            };
            BTC_cargo_lifted = ObjNull;            
            waitUntil {getPos _payload select 2 < 0.1};
            detach _payload;
            _payload setVectorUp surfaceNormal position _payload;            
        };
};

BTC_Fnc_SinglePara = {
        private ["_para","_paras","_payload","_smoke","_paradeployed"];
        _paradeployed = 1;
        BTC_lifted = 0;
        BTC_cargo_lifted setvariable ["BTC_cannot_lift",1,false];
        waitUntil {BTC_cargo_lifted distance _chopper > 25};
        BTC_cargo_lifted setvariable ["BTC_cannot_lift",0,false];            
        _para = createVehicle ["B_Parachute_02_F", [0,0,0], [], 0, "FLY"];
        _para setDir getDir _this;
        _para setPos getPos _this;
        _paras =  [_para];
        _this attachTo [_para, [0,0,0]];
        0 = [_this, _paras] spawn {
            _payload = _this select 0;
            // Wait until near ground then deploy smoke but only if there is no one (alive) inside.
            waitUntil {(getPos _payload select 2) < 25};
            if ({alive _x} count crew _payload == 0)
            then {
            _smoke = "SmokeShellGreen" createVehicle position _payload;
            _smoke setPos (getPos _payload);
            _smoke attachTo [_payload,[0,0,0]];
            };
            BTC_cargo_lifted = ObjNull;            
            waitUntil {getPos _payload select 2 < 0.1};
            detach _payload;
            _payload setVectorUp surfaceNormal position _payload;
        };
};
//Adapted from Killzone Kid's 'Epic Armour Drop' Script
BTC_Fnc_MultiPara = {
        private ["_para","_paras","_p","_payload","_time","_smoke","_paradeployed"];
        _paradeployed = 1;
        BTC_lifted = 0;
        BTC_cargo_lifted setvariable ["BTC_cannot_lift",1,false];
        waitUntil {BTC_cargo_lifted distance _chopper > 25};
        BTC_cargo_lifted setvariable ["BTC_cannot_lift",0,false];            
        _para = createVehicle ["B_Parachute_02_F", [0,0,0], [], 0, "FLY"];
        _para setDir getDir _this;
        _para setPos getPos _this;
        _paras =  [_para];
        _this attachTo [_para, [0,2,0]];
        {
            _p = createVehicle ["B_Parachute_02_F", [0,0,0], [], 0, "FLY"];
            _p disableCollisionWith _chopper;
            _paras set [count _paras, _p];
            _p attachTo [_para, [0,0,0]];
            _p setVectorUp _x;
        } count [
            [0.5,0.4,0.6],[-0.5,0.4,0.6],[0.5,-0.4,0.6],[-0.5,-0.4,0.6]
        ];
        0 = [_this, _paras] spawn {
            _payload = _this select 0;
            // Wait until near ground then deploy smoke but only if there is no one (alive) inside.
            waitUntil {(getPos _payload select 2) < 25};
            if ({alive _x} count crew _payload == 0)
            then {
            _smoke = "SmokeShellGreen" createVehicle position _payload;
            _smoke setPos (getPos _payload);
            _smoke attachTo [_payload,[0,0,0]];
            };
            BTC_cargo_lifted = ObjNull;            
            waitUntil {getPos _payload select 2 < 1};
            detach _payload;
            _payload setVectorUp surfaceNormal position _payload;
            {
                detach _x;
                _x disableCollisionWith _payload;
            } count (_this select 1);
            _time = time + 5;
            waitUntil {time > _time};
            {
                if (!isNull _x) then {deleteVehicle _x};
            } count (_this select 1);
        };
};
};