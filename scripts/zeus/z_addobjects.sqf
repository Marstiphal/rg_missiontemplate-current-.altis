//Loop which adds objects for the zeus to edit
//Done by eRazeri & Magoo
//execute from init.sqf: execVM "scripts\zeus\z_addobjects.sqf";
if (isServer) then 
{
sleep 6;

{
_x addCuratorEditableObjects [playableunits + switchableunits - (entities "VirtualMan_F"),true];//Adds players for zeus
_x call bis_fnc_drawCuratorLocations;//Label every vilage, town and city in curator interface
[_x,[-1,-2,0]] call BIS_fnc_setCuratorVisionModes;//Adds thermal
[_x,true] call bis_fnc_moduleRespawnInventory;//Allows curator to edit all respawn loadouts
} foreach allCurators;

fnc_admin_1_zeus = {
      {
      Admin_1_z addCuratorEditableObjects [[_x],true];
      } forEach nearestObjects [position Admin_1, ["AllVehicles","Man","thingX"], 400];  
      };
fnc_admin_2_zeus = {
      {
      Admin_2_z addCuratorEditableObjects [[_x],true];
      } forEach nearestObjects [position Admin_2, ["AllVehicles","Man","thingX"], 400];  
      };      
fnc_admin_3_zeus = {
      {
      Admin_3_z addCuratorEditableObjects [[_x],true];
      } forEach nearestObjects [position Admin_3, ["AllVehicles","Man","thingX"], 400];  
      };
fnc_admin_4_zeus = {
      {
      Admin_4_z addCuratorEditableObjects [[_x],true];
      } forEach nearestObjects [position Admin_4, ["AllVehicles","Man","thingX"], 400];  
      };
                  
while {true} do 
   {     
   if (!isNil "Admin_1") then {
   call fnc_admin_1_zeus
   };
   if (!isNil "Admin_2") then {
   call fnc_admin_2_zeus
   };
   if (!isNil "Admin_3") then {
   call fnc_admin_3_zeus
   };
   if (!isNil "Admin_4") then {
   call fnc_admin_4_zeus
   };
   sleep 10;    
   };
};