//Init made for the cargo ramp thingy by eRazeri
{
if (
(typeOf _x == "RG_Merlin_CTRG")
||
(typeOf _x == "RG_Merlin_DS")
||
(typeOf _x == "RG_Merlin_WD")
) then {
if (isNil {player getVariable "usableCargoRamp_functions_initialized"}) then {
execVM "scripts\usableCargoRamp\functions.sqf";
};
[_x] execVM "scripts\usableCargoRamp\merlin.sqf";
};


if (
(typeOf _x == "MV22_ported")
) then {
if (isNil {player getVariable "usableCargoRamp_functions_initialized"}) then {
execVM "scripts\usableCargoRamp\functions.sqf";
};
[_x] execVM "scripts\usableCargoRamp\osprey.sqf";
};

if (
(typeOf _x == "C130J_ported")
) then {
if (isNil {player getVariable "usableCargoRamp_functions_initialized"}) then {
execVM "scripts\usableCargoRamp\functions.sqf";
};
[_x] execVM "scripts\usableCargoRamp\hercules.sqf";
};


if (
(typeOf _x == "CH_47F_EP1")
) then {
if (isNil {player getVariable "usableCargoRamp_functions_initialized"}) then {
execVM "scripts\usableCargoRamp\functions.sqf";
};
[_x] execVM "scripts\usableCargoRamp\chinook.sqf";
};


if (
(typeOf _x == "RG_Ghosthawk_CTRG")
||
(typeOf _x == "RG_Ghosthawk_DS")
||
(typeOf _x == "RG_Ghosthawk_WD")
) then {
if (isNil {player getVariable "usableCargoRamp_functions_initialized"}) then {
execVM "scripts\usableCargoRamp\functions.sqf";
};
[_x] execVM "scripts\usableCargoRamp\ghosthawk.sqf";
};

if (
(typeOf _x == "RG_Wildcat_CTRG")
||
(typeOf _x == "RG_Wildcat_DS")
||
(typeOf _x == "RG_Wildcat_WD")
||
(typeOf _x == "RG_Wildcat_unarmed_CTRG")
||
(typeOf _x == "RG_Wildcat_unarmed_DS")
||
(typeOf _x == "RG_Wildcat_unarmed_WD")
) then {
if (isNil {player getVariable "usableCargoRamp_functions_initialized"}) then {
execVM "scripts\usableCargoRamp\functions.sqf";
};
[_x] execVM "scripts\usableCargoRamp\wildcat.sqf";
};


} forEach vehicles;