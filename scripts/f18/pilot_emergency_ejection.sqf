//Automatic ejection for F/A-18 E/F & Wipeout Pilot incase the jet is damaged beyond repair
//script by eRazeri
if ((typeOf (vehicle player) == "RG_PilotJet_DS") || (typeOf (vehicle player) == "RG_PilotJet_WD") || (typeOf (vehicle player) == "RG_PilotLead_WD") || (typeOf (vehicle player) == "RG_PilotLead_DS")) 
then {
while {true} do {
while {(typeOf (vehicle player) == "JS_JC_FA18E") || (typeOf (vehicle player) == "JS_JC_FA18F") || (typeOf (vehicle player) == "B_Plane_CAS_01_F") || (typeOf (vehicle player) == "RG_Warthog")} do {

      
  if (((getPosATL player select 2) > 20) && ((getPosASL player select 2) > 20)) then {
      player allowDamage false;  
    } else {
		  player allowDamage true;
	};
            
  if ((alive player) && (damage vehicle(player) > 0.99) && (typeOf (vehicle player) == "JS_JC_FA18E")) then {
        
      player allowdamage false;
      (vehicle player) execVM "\js_jc_fa18\scripts\EJECTION\FA18_Ejection_E.sqf";
      player setDamage 0.349;
      sleep 2;      
      player allowdamage true;
      sleep 60;
    };
  if ((alive player) && (damage vehicle(player) > 0.99) && (typeOf (vehicle player) == "JS_JC_FA18F")) then {
        
      player allowdamage false;
      (vehicle player) execVM "\js_jc_fa18\scripts\EJECTION\FA18_Ejection_F.sqf";
      player setDamage 0.349;
      sleep 3;      
      player allowdamage true;
      sleep 60;
    };
    
    if ((alive player) && (damage vehicle(player) > 0.99) && ((typeOf (vehicle player) == "B_Plane_CAS_01_F") || (typeOf (vehicle player) == "RG_Warthog"))) then {
        
      player setDamage 0.349;
      player allowdamage false;
      
      sleep .05;
      
      player action ["eject",vehicle player];
     
     if (((getPosATL player select 2) > 100) && ((getPosASL player select 2) > 100)) then {
     
      waitUntil {sleep .1; (((getPosATL player select 2) < 50) || ((getPosASL player select 2) < 50))};    
      } else {      
      waitUntil {sleep .1; (((getPosATL player select 2) < 30) || ((getPosASL player select 2) < 30))};		  
	    };

      _para = "Steerable_Parachute_F" createVehicle (position player);
      _para setPos (getPos player);
      
      sleep .05;

      player moveInDriver _para;

      sleep .05;
      
      player allowdamage true; 
      sleep 60;   
    };

    
  sleep .05;//20m*20x/s=400m/s=1440km/h
};
sleep 10;  
};
};