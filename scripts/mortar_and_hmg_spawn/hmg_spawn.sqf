_veh = _this select 0;
_player = _this select 1;
_action = _this select 2;

_pos = _player modeltoworld [0,2,0];
_pos set [2,0];
_dir = direction _player;

if(!isNull _player) then{

if(_player != _veh) exitWith{hint "Stop touching my stuff!"; sleep 5; hint "";};
if(vehicle _player != _player) exitWith {hint "Can't deploy HMG inside vehicle"; sleep 5; hint"";};

if (surfaceIsWater _pos) exitWith {
	hint "It is not possible to deploy the HMG in water.";
	sleep 5; hint "";
};

_player playMove "AinvPknlMstpSlayWrflDnon_medic";
sleep 5;
waitUntil {animationState _player != "AinvPknlMstpSlayWrflDnon_medic"};
_player playMove "AinvPknlMstpSlayWrflDnon_medic";
sleep 5;
waitUntil {animationState _player != "AinvPknlMstpSlayWrflDnon_medic"};
_player playMove "AinvPknlMstpSlayWrflDnon_medic";
sleep 5;
waitUntil {animationState _player != "AinvPknlMstpSlayWrflDnon_medic"};
_player playMove "AinvPknlMstpSlayWrflDnon_medic";
sleep 5;
waitUntil {animationState _player != "AinvPknlMstpSlayWrflDnon_medic"};

if (!alive _player) exitWith {hint "You died before your HMG was ready."; sleep 5; hint "";};

_vehicle_0 = objNull;
if (true) then
{
  _this = createVehicle ["RG_HMG", _pos, [], 0, "NONE"];
  _vehicle_0 = _this;
  _this setDir _dir;
  _this setPos _pos;
  {_this removeMagazine _x} forEach magazines _this;
  {_this addMagazine _x} forEach hmg_paukku_array;
  _this addAction["<t color='#ff1111'>Remove HMG</t>", "scripts\mortar_and_hmg_spawn\hmg_remove.sqf", nil, 1, false, True, "", "_this == HMG_1 || _this ==   HMG_2 || _this ==  HMG_3 || _this ==  HMG_4"];
  HMG_Spawned = true;
  
};

hint "HMG completed, Use 'Remove HMG' action instead of dissassemble!";
player removeAction _action;
};