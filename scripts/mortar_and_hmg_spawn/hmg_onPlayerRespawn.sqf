private ["_unit","_corpse"];

_unit = _this select 0;
_corpse = _this select 1;

if ((local _unit) && !(HMG_Spawned)) then {
	_corpse removeAction HMG_Spawn_Action;
	
			HMG_Spawn_Action = player addAction ["<t color='#ff1111'>Deploy HMG","scripts\mortar_and_hmg_spawn\HMG_spawn.sqf", nil, 1, false, True, "","_this == HMG_1 || _this ==  HMG_2 || _this ==  HMG_3 || _this ==  HMG_4"];
};