if (hasInterface) then {
private ["_recoil_users_list","_recoil_vehicles_list","_recoil_weapons_list"];
    _recoil_users_list =
    [//list of classnames able to use this script, if left empty, runs on everyone  
    	"RG_PilotLead_DS",
			"RG_PilotJet_DS",
			"RG_PilotHeli_DS",
      "RG_PilotLead_WD",
			"RG_PilotJet_WD",
			"RG_PilotHeli_WD"
    ];
    
    _recoil_vehicles_list =
    [//vehicle classnames able to use this script, if left empty, runs on all planes
    "RG_Warthog",
    "B_Plane_CAS_01_F"
    ];
    
    plane_recoil_weapons_list =
    [//weapons that cause the rumble :P
    "Gatling_30mm_RG_Warthog",
    "Gatling_30mm_Plane_CAS_01_F"  
    ];
    
if ((typeOf (vehicle player) in _recoil_users_list) || (count _recoil_users_list == 0)) then {
while {true} do {
if ((typeOf (vehicle player) in _recoil_vehicles_list) || (((vehicle player) isKindOf "plane") && (count _recoil_vehicles_list == 0))) then {
_vehicle = (vehicle player);
_FiredHandleri = _vehicle addEventHandler ["Fired", {
_wep = _this select 1;
if (_wep in plane_recoil_weapons_list) then {
_veh = _this select 0;                                                                  
_vel = velocity _veh;
_dir = _veh weaponDirection _wep;
_veh setVelocity [
(_vel select 0) + (_dir select 0) * -0.3,
(_vel select 1) + (_dir select 1) * -0.3,
(_vel select 2) + (_dir select 2) * -0.3 + 0.3
];
_burst = getNumber (configfile >> "CfgWeapons" >> _wep >> "burst");
_reload_time = getNumber (configfile >> "CfgWeapons" >> _wep >> "reloadTime");        
_power = 0.5;
_duration = _reload_time * _burst;
_frequency = 1/_reload_time;
addCamShake [_power,_duration,_frequency];
};
}];
waitUntil {sleep 2; !((vehicle player) == _vehicle)};
_vehicle removeEventHandler ["Fired", _FiredHandleri];
};
sleep 11;
}; 
};
};