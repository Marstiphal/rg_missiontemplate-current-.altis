if (hasInterface) then {
  private ["_recoil_users_list","_recoil_vehicles_list","_recoil_weapons_list"];
    _recoil_users_list=
    [//list of classnames able to use this script, if left empty, runs on everyone  
      "RG_ExcaliburLead_DS",
      "RG_ExcaliburGun_DS",
      "RG_ExcaliburDrv_DS",
      "RG_ExcaliburCom_DS",
      "RG_ExcaliburLead_WD",
      "RG_ExcaliburGun_WD",
      "RG_ExcaliburDrv_WD",
      "RG_ExcaliburCom_WD" 
    ];
    
    _recoil_vehicles_list=
    [//vehicle classnames able to use this script, if left empty, runs on all planes
    "RG_Slammer_CTRG",
    "RG_Slammer_UP_CTRG",
    "RG_Scorcher_CTRG",
    "RG_Kuma_CTRG",
    "RG_Sandstorm_CTRG"
    ];
    
    tank_recoil_weapons_list=
    [//weapons that cause the rumble :P    
    "cannon_105mm",
    "cannon_120mm",
    "cannon_120mm_long",
    "mortar_155mm_AMOS",
    "rockets_230mm_GAT" 
    ];

if ((typeOf (vehicle player) in _recoil_users_list) || (count _recoil_users_list == 0)) then {
while {true} do {
if ((typeOf (vehicle player) in _recoil_vehicles_list) || (((vehicle player) isKindOf "tank") && (count _recoil_vehicles_list == 0))) then {
if (player == gunner (vehicle player)) then {
_vehicle = (vehicle player);
_FiredHandleri = _vehicle addEventHandler ["Fired", {
_wep = _this select 1;
if (_wep in tank_recoil_weapons_list) then {
_veh = _this select 0;
_vel = velocity _veh;
_dir = _veh weaponDirection _wep;
_veh setVelocity [
(_vel select 0) + (_dir select 0) * -5,
(_vel select 1) + (_dir select 1) * -5,
(_vel select 2) + (_dir select 2) * -5
];
};
}];
waitUntil {sleep 1; (!(player == gunner _vehicle))};
_vehicle removeEventHandler ["Fired", _FiredHandleri];
};
};
sleep 1;
}; 
}; 
};