_allowedWeapons = [
"Rocket_04_HE_RG_Warthog",
"Rocket_04_AP_RG_Warthog",
"Gatling_30mm_RG_Warthog",
"Bomb_04_RG_Warthog"];

//weapon memory position name
_gatlinInfo = {
	_this selectionPosition "Gatling_barrels_end";
};

_heRocketInfo = {
	[5.3,1.5,-0.5];
};

_apRocketInfo = {
	[-5.3,1.5,-0.5];
};

_bombInfo = {
	[0,0,-1];
};

_pairs = [];
_pairs = [_pairs,"Bomb_04_RG_Warthog",_bombInfo,false] call BIS_fnc_addToPairs;
_pairs = [_pairs,"Gatling_30mm_RG_Warthog",_gatlinInfo,false] call BIS_fnc_addToPairs;
_pairs = [_pairs,"Rocket_04_HE_RG_Warthog",_heRocketInfo,false] call BIS_fnc_addToPairs;
_pairs = [_pairs,"Rocket_04_AP_RG_Warthog",_apRocketInfo,false] call BIS_fnc_addToPairs;

[_allowedWeapons , _pairs];
