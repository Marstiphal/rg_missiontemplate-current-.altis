//This script removes/adds time on map based on if you have a watch or not.
//Script by KK
player addEventHandler ["Put", {
    if (_this select 2 == "ItemWatch") then {
        findDisplay 12 displayCtrl 101 ctrlShow (
            if (_this select 2 in assignedItems player) then [
                {true},
                {false}
            ]
        )
    }
}];
player addEventHandler ["Take", {
    if (_this select 2 == "ItemWatch") then {
        findDisplay 12 displayCtrl 101 ctrlShow (
            if (_this select 2 in assignedItems player) then [
                {true},
                {false}
            ]
        )
    }
}];