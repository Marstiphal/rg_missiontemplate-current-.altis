MissionTemplate
===============

RG Template mission 

This repository contains mission templates for the Gaming Community Reality Gaming. 
This is not intended as a finished mission repository, but is here as a collaborative tool
for the bare bones mission template for mission makers of the community to build from.

Things we change here is:

1. Player slots sorting and naming.
2. Script Packs
3. MP mission nessesary files such as init.sqf, initserver.sqf, mission.sqm etc.

Known Bugs/to do (needs fixing):

-You can't load injured people in vehicles. (will be fixed by switching over to the AGM medical system)

-Adapt my (eRazeri) vehicle spawn system from PDT to make a vehicle respawn system which checks that the area is clear...