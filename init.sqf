//Disable that annoying saving stuff I hate so much
enableSaving[false,false];

//makes players to wait for being initialized
if (!isServer && isNull player) then {isJIP=true;} else {isJIP=false;};
if (hasInterface) then {waitUntil {sleep 0.01; !isNull player && isPlayer player};};

[ //This is the settings array for qbt optimizer
	[
		// General settings
		5,	    // Time between loops (default 5s)
		false,	// Enable log file
		30,	    // Write to log interval (default 30s)		
		false	  // Enable debug
	],

	[
		// Server settings, these are runned only on server side, -1 to disable feature
		300,	// WeaponHolder stay time		
		900,	// Immobile/destroyed vehicle stay time		
		300,	// Dead body stay time		
		900,	// Explosive stay time		
		3600,	// Mine stay time		
		600,	// Chemlight (& SmokeShell) stay time (class SmokeShell includes chemlights as well)		
		-1    // Disable unneeded AI features (FSM) distance from nearest player
	],

	[
		// Client settings, these are runned only on client side, -1 to disable feature		
		-1,	// Player & AI simulation distance, stops updating locations on clients -> messes up with acre comms & ctab
		-1	// Distance where to find static objects to disable simulation of (qbt recommends 500), increase to save bandwidth, beware of issues it might cause
	]
] execVM "scripts\qbt_optimizer.sqf";

//Admin commands 
execVM "scripts\AdminLoop\init.sqf"; 

//Vehicle repair point script
execVM "scripts\gvs\gvs_init.sqf";

//Suppression Script
_null = [2] execVM "scripts\tpwcas\tpwcas_script_init.sqf";

[//setSkillz & nvg removal & GPS adder, looper designed for ALiVE & Zeus by eRazeri
0,//AI Skill settings, check the script for the exact skill settings
0,//Opfor nvg removal
0,//Blufor nvg removal
0,//RG nvg removal
0,//Add GPS to player if he hasnt got one
0,//Enable rpt logging
0 //Enable Extensive Skill rpt logging for debugging etc
] execVM "scripts\difficulty\AI_and_player_difficulty.sqf";

//Lock players in first person when moving/shooting etc
execVM "scripts\3rd_person_limiter\init.sqf";

//Execute and load briefing
execVM "briefing.sqf";

//Vehicle Active Defence System
call compile preprocessFile "scripts\Baked_AIS\Baked_AIS_init.sqf";

// BTC Logistics (lift capabilities for helicopters etc.)
_logistic = execVM "scripts\=BTC=_Logistic\=BTC=_logistic_Init.sqf";

// INS_revive initialize (medical system)
//[] execVM "scripts\INS_revive\revive_init.sqf";

//Group manager, dependant on menu extension caller script and addon
//execVM "scripts\group_manager\group_manager.sqf";

//DOM field repair script which allows engineers to repair vehicles
execVM "scripts\DOM_repair\init.sqf";

//Mortar & HMG script for Gambler detachments
execVM "scripts\mortar_and_hmg_spawn\mortar_init.sqf";
execVM "scripts\mortar_and_hmg_spawn\hmg_init.sqf";

//Automatic ejection for F/A-18/Wipeout Pilot in case the jet is damaged beyond repair
execVM "scripts\f18\pilot_emergency_ejection.sqf";

[//ACRE customization
1,//Enable terrain loss, default 1
0.5,//Terrain loss value, suggested value 0.5, default is 1
1,//Disable in game automatic messages, suggested value 1
1//Enable retransmission network, depends on if you have antennas on the mission and want to use them.
] execVM "scripts\acre\acre_init.sqf";

//Recoil scripts (awesome!)
execVM "scripts\recoil\tank_recoil.sqf";//Script runs only for Excalibur classnames
execVM "scripts\recoil\a10_recoil.sqf";//Script runs only for Talon Jet Pilots

//Add all objects, vehicles & players for Zeus to edit within range of 400m
execVM "scripts\zeus\z_addobjects.sqf";

//alive Zeus friendship
execVM "scripts\zeus\z_alive_friendship.sqf";

//Removes build number if it shows up after using Zeus/Splendid camera
execVM "scripts\UI\build_number.sqf";

//Work in progress RG Task Force Tracker
execVM "scripts\tracker\init.sqf";

//This script removes/adds time on map based on if you have a watch or not.
execVM "scripts\time\watch.sqf";

//Opening doors for helicopters, moved init from unit init
execVM "scripts\helidoors\init.sqf";

//Usable Cargo Ramp/beep tones/interior lights for Helos
execVM "scripts\usableCargoRamp\init.sqf";

[//Mission difficulty checker that announces if using a non standard difficulty
false//Enable debugging
]execVM "scripts\difficulty\mission_difficulty.sqf";

//CCIP Script
execVM "scripts\ccip\init.sqf";